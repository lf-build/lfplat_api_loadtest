#!/bin/bash

ReportLoc="./report_files"
FolderName=$(date +%Y%m%d)
mkdir -p "./output"/${FolderName}/"logs"
OutputDir="./output"/${FolderName}
OutPutLogs="./output"/${FolderName}/"logs"
PROPERTY_FILE="./env.properties"
TestsConfigDir="./test_config"
ArrivalRate=1
head=`cat ${ReportLoc}/head.txt`
rest=`cat ${ReportLoc}/rest.txt`
NewJson='{"timestamp":"_","duration":"_","result":[]}'
