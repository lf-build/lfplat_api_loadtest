
---------------------------------------------------------------------------------------------------------------------------------------------------
* Pre-requisite *
---------------------------------------------------------------------------------------------------------------------------------------------------

- Install npm.
	sudo curl -s https://deb.nodesource.com/gpgkey/nodesource.gpg.key | sudo apt-key add -
	sudo sh -c "echo deb https://deb.nodesource.com/node_8.x bionic main /etc/apt/sources.list.d/nodesource.list"
	sudo apt-get update
	sudo apt-get install nodejs
 
- Install Artillery
	npm install artillery

- Install JP
        sudo apt-get install jq
------------------------------------------------------------------------------------------
* TO RUN THE SCRIPT *
------------------------------------------------------------------------------------------
 
Step 1 : Navigate to the project Folder (Ex : home/sigma/lfplat_api_loadtest) in CMD.

Step 2 : Run ./init.sh.

Step 3 : Once the Test Execution is completed you will find the summary report in the output/[YYYYMMDD]/Report.html.

*Note* : You can view the ARTILLERY logs in the log folder and each of these logs can be converted to reports by using [ artillery report [JSON_FileName.json]].

---------------------------------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
* CONFIGURABLE PARAMETERS *
------------------------------------------------------------------------------------------
 
Step 1 : Open the properties file in a text editor (Ex : home/sigma/lfplat_api_loadtest/env.properties).

Below are the description for each key :
		
		hostUrl				  	    : Set the host URL here. Exclude the Ports
		token				        : Set the Tenant Token here.
		duration			 	    : Indicates the Duration for each test in seconds. (Ex: duration=5, each iteration will be executed for 5secs).
		NoofUserPerSecond			: Indicates the Number of User per second (>1 indicate the total number of users per sec, <1 Ex:0.5 indicates half
		                              user per sec which is 1user every 2secs).
		limit				   	 	: Scripts will excute only till the value provided here and stops once it exceeds.
		benchmark			    	: Indicates when the individual test should stop executing(Ex: benchmark=100, then it will start the next iteration 
		                              only when it satisfies the condition if not it stops and continues with the next).
		iterator			    	: Indicates the value to be added or multipled (Ex : iterator:1, then it will add or multilply 1 to the User) 
		operation			        : Indicates the operation to be carried out (Supports add/multiply)
		generateIndividualReports	: If set true will generate ARTILLERY reports for each test runs. Can be set to false if not required.

---------------------------------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
* ADDING TEST SCIPTS *
------------------------------------------------------------------------------------------
 
Step 1 : Construct the required yaml file and place it within the test_config location (Ex : home/sigma/lfplat_api_loadtest/test_config).

Step 2 : Follow the file naming convention ServiceName_ScenarionName.

Step 3 : Add the corresponding port in the property file with [ScenarioName]Port=0000.

Step 4 : If the script uses any JS scripts add it to the "js" folder.

Step 5 : Follow "* TO RUN THE SCRIPT *".

---------------------------------------------------------------------------------------------------------------------------------------------------


