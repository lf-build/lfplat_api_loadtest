#!/bin/bash

source functions.sh
source globals.sh
source ${PROPERTY_FILE}
limit=$(($limit+1))

 #Get the Service to be tested from the service Array
 for entry in "$TestsConfigDir"/*
 do
   if [[ ${entry} =~ .*\.yml$ ]];then
    ArrivalRate=${NoofUserPerSecond}
    ScenarioName="${entry:14:-4}"
    Service=${ScenarioName%_*}
    echo "#################################################################################################################################"
    echo "Service : ${Service}"
    echo "Scenario : ${ScenarioName}"
    url=${hostUrl}":"$(getProperty ${Service}"Port")
    echo "URL : $url"
    echo "Token : Bearer $token"
    echo  -e "#################################################################################################################################\n\n"
     #compares the Arrival Rate with the Limit Value From the Property File
      while [  $((ArrivalRate)) -lt $((limit)) ]
      do
       #start the run
         #DEBUG=http*
         artillery -t ${url} run --overrides '{"config": {"phases": [{"duration": '${duration}',"arrivalRate": '${ArrivalRate}'}],"defaults": {"headers": {"Authorization": "Bearer '${token}'"}}}}' -o ${OutPutLogs}/${ScenarioName}_${ArrivalRate}.json ${entry}
         sleep 2s
         #Get the values and compute the aggregate from the logs JSON
         Aggregate=$(getAggregater ${OutPutLogs}/${ScenarioName}_${ArrivalRate}.json)
         echo "Aggregate : "${Aggregate}
         #Checks the Aggregate against the Benchmark from Prop File and decides to increase the Arrival Rate or Stops the Execution
        if [[ $(bc <<< "${Aggregate}>=$((benchmark))") -eq 1 ]]
          then
            generateJSON ${ScenarioName} ${ArrivalRate} ${Aggregate} ${OutPutLogs}/${ScenarioName}_${ArrivalRate}.json
            if [ "${operation}" = "multiply" ]
             then
               ArrivalRate=$((ArrivalRate*iterator))
             else
               ArrivalRate=$((ArrivalRate+iterator))
            fi
            echo "New Arrival Rate : "${ArrivalRate}
          else
            generateJSON ${ScenarioName} ${ArrivalRate} ${Aggregate} ${OutPutLogs}/${ScenarioName}_${ArrivalRate}.json
            ArrivalRate=${limit}
        fi
      done
    fi
 done

generateReport $(timestamp) ${duration}


