#!/bin/bash

source globals.sh

function getProperty {
   PROP_VALUE=`cat ${PROPERTY_FILE} | grep "$1" | cut -d'=' -f2`
   echo ${PROP_VALUE}
}

function getAggregater {
 ScenarioCount=`jq '.aggregate.scenariosCompleted' $1`
 ScenarioCreated=`jq '.aggregate.scenariosCreated' $1`
 node -pe $((ScenarioCount))/$((ScenarioCreated))*100
}

function generateReport {
 NewJson=`echo ${NewJson} | jq '.timestamp="'$1" "$2'"' |jq '.duration="'$3'"'`
 echo "${head}" > ${OutputDir}/Report.html
 echo "window.response=$NewJson;" >> ${OutputDir}/Report.html
 echo "${rest}" >> ${OutputDir}/Report.html
 sleep 2s
 google-chrome ${OutputDir}/"Report.html"
 generateIndividualReport
}

function generateIndividualReport {
 check=$(getProperty "generateIndividualReports")
 if ${check}; then
   for entry in "$OutPutLogs"/*
   do
    if [[ $entry =~ .*\.json$ ]];then
        artillery report "$entry"
    fi
   done
 fi
}

function getCodes {
  if [ ${1%.*} -eq "100" ]
  then
      code=`jq '.aggregate.codes' $2`
      echo $code >> tempA.json
      scnd=`jq '.aggregate.scenarioDuration' $2`
      echo $scnd >> tempB.json
  else
      newCodes=`jq '.aggregate.codes' $2`
      echo $newCodes >> tempA.json
      erros=`jq '.aggregate.errors' $2`>> tempB.json
      echo $erros >> tempB.json
  fi
  codes=`jq -s add ./tempA.json ./tempB.json`
  rm -r ./tempA.json ./tempB.json
  echo ${codes}
  }

function generateJSON {
 sampleJson=`jq -n --arg scenario "$1" --arg aggregate "$3" --arg arrivalrate "$2" --argjson err "$(getCodes "$3" "$4")" '{"ScenarioName": $scenario, "Aggregate": $aggregate, "ArrivalRate": $arrivalrate, "codes" : $err}'`
 NewJson=`echo ${NewJson}| jq ".result[.result|length] |= .+ ${sampleJson}"`
}

timestamp() {
  echo $(date +"%Y-%m-%d %T")
}

