var fs = require('fs');
var id = 0;

module.exports = {
    generateLoanNumber: generateLoanNumber,
    generateUserNameLoanNumber:generateUserNameLoanNumber,
    generateUserName:generateUserName,
    setHeaderSecurityIdentity:setHeaderSecurityIdentity,
    UploadDocMultipart:UploadDocMultipart,
    generateLookUpName:generateLookUpName
}

function generateLoanNumber(requestParams, context, ee, next) {
    id += 1;
    var LoanNumber = '0000000' + id;
    context.vars['LoanNumber'] = 'AB' + LoanNumber.slice(-7);
    return next();
}

function generateUserNameLoanNumber(requestParams, context, ee, next) {
    id += 1;
    var LoanNumber = '0000000' + id;
    context.vars['LoanNumber'] = 'AB' + LoanNumber.slice(-7)
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 4; i++){
     text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    context.vars['UserName'] = 'User_' + text;
    return next();
}

function generateUserName(requestParams, context, ee, next) {
   var text = "";
   var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
   for (var i = 0; i < 4; i++){
    text += possible.charAt(Math.floor(Math.random() * possible.length));
   }
   context.vars['UserName'] = 'User_' + text;
   return next();
}

function generateLookUpName(requestParams, context, ee, next) {
   var text = "";
   var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
   for (var i = 0; i < 4; i++){
    text += possible.charAt(Math.floor(Math.random() * possible.length));
   }
   context.vars['LookUpName'] = 'LookUp_' + text;
   return next();
}

function setHeaderSecurityIdentity(requestParams, context, ee, next){
  requestParams.headers.authorization="Bearer " + context.vars['LoginToken'];
  return next();
}

function UploadDocMultipart(requestParams, context, ee, next){
var url = 'http://10.100.0.13:7013/application/My0001';
var headers = { 
    'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJteS1hcHAiLCJpYXQiOiIyMDE1LTExLTE0VDAwOjUxOjE2LjI0MTczNTZaIiwiZXhwIjoiMjAxNi0xMS0xNFQwMDo1MDo1Ni43Njc0NzY0WiIsInN1YiI6ImpvaG5zbWl0aCIsInRlbmFudCI6Im15LXRlbmFudCIsInNjb3BlIjpbImFtb3J0aXphdGlvbi9zY2hlZHVsZSIsImxvYW5zL2RvY3VtZW50cy91cGxvYWQiXSwiSXNWYWxpZCI6ZmFsc2V9.F_0aDeBgLpNGBiD5IVVAgEN07swGpnbhg8-yTWFzqX0',
    'Content-Type' : 'application/x-www-form-urlencoded' 
};

var readStream = fs.createReadStream('../files/Driving_License.jpg');
readStream.on('error', function () {
      console.log("Some error with the file");
});
readStream.on('open', function () {
      console.log("File has been Read");
});


var form = { file: readStream, fileName: 'DL', tags: 'BO'};
var jsonObject;
request.post({ url: url, formData : form, headers: headers }, function (e, r, body) {
   jsonObject = JSON.parse(r.body)
});

 context.vars['DocumentID'] = jsonObject.documentId;
}
